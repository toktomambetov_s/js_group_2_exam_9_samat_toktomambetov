import React, { Component, Fragment } from 'react';
import "./Contacts.css";
import {connect} from 'react-redux';

import Contact from "../../components/Contact/Contact";
import Modal from "../../components/UI/Modal/Modal";
import {fetchContacts} from "../../store/actions";
import ContactInfo from "../../components/ContactInfo/ContactInfo";

class Contacts extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: '',
        id: '',
        index: null,
        show: false,
    };

    componentDidMount() {
        this.props.fetchContacts();
    }

    showModalHandler = (id, index) => {
        console.log(this.props.contacts[index]);
        this.setState({
            name: this.props.contacts[index].name,
            phone: this.props.contacts[index].phone,
            email: this.props.contacts[index].email,
            photo: this.props.contacts[index].photo,
            id: this.props.contacts[index].id,
            index: index,
            show: true
        });
    };

    hideModalHandler = () => {
        this.setState({show: false});
    };

    closeModalHandler = () => {
        this.setState({show: false});
    };

    render() {
        return (
            <Fragment>
                <Modal show={this.state.show}
                       closed={this.hideModalHandler}
                >
                    <ContactInfo
                        name={this.state.name}
                        phone={this.state.phone}
                        email={this.state.email}
                        photo={this.state.photo}
                        // delete={this.props.deleteContact(this.state.id, this.state.index)}
                        close={this.closeModalHandler}
                    />
                </Modal>
                <section className="Contacts">
                    {this.props.contacts.map((contact, index) => (
                        <Contact name={contact.name}
                                 photo={contact.photo}
                                 click={() => this.showModalHandler(contact.id, index)}
                        />
                    ))}
                </section>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        fetchContacts: () => dispatch(fetchContacts()),
        // deleteContact: (id, index) => dispatch(deleteContact(id, index))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
import React, { Component, Fragment } from 'react';
import Form from "../../components/Form/Form";

import axios from '../../axios-contacts';

class AddContactForm extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: '',
    };

    changeContactName = e => this.setState({name: e.target.value});
    changeContactPhone = e => this.setState({phone: e.target.value});
    changeContactEmail = e => this.setState({email: e.target.value});
    changeContactPhoto = e => this.setState({photo: e.target.value});

    addContact = event => {
        event.preventDefault();
        const contact = {
            name: this.state.name,
            phone: this.state.phone,
            email: this.state.email,
            photo: this.state.photo,
        };

        axios.post('/contacts.json', contact).finally(() => {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h1>Add new contact</h1>
                <Form contactName={this.changeContactName} contactPhone={this.changeContactPhone} contactEmail={this.changeContactEmail} contactPhoto={this.changeContactPhoto} photoSrc={this.state.photo} click={this.addContact}/>
            </Fragment>
        );
    }
}

export default AddContactForm;
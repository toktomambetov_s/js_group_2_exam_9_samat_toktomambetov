import React, { Fragment } from 'react';
import './ContactInfo.css';

const ContactInfo = props => {
    return (
        <Fragment>
            <div className="ContactInfo">
                <img className="ContactInfoPhoto" src={props.photo} alt=""/>
                <div className="Info">
                    <h3>{props.name}</h3>
                    <p>Phone: +{props.phone} </p>
                    <p>Email: {props.email}</p>
                    <button>Edit</button>
                    <button onClick={props.delete}>Delete</button>
                    <button onClick={props.close}>Close</button>
                </div>
            </div>
        </Fragment>
    )
};

export default ContactInfo;
import React from 'react';
import './Form.css';

const Form = props => {
    return (
        <div>
            <form className="Form">
                <div className="form-group">
                    <label>Name</label>
                    <input onChange={props.contactName} type="text" className="form-control" id="exampleFormControlInput1" placeholder="Name"/>
                </div>
                <div className="form-group">
                    <label>Phone</label>
                    <input onChange={props.contactPhone} type="number" className="form-control" id="exampleFormControlInput1" placeholder="Phone"/>
                </div>
                <div className="form-group">
                    <label>Email</label>
                    <input onChange={props.contactEmail} type="email" className="form-control" id="exampleFormControlInput1" placeholder="Email"/>
                </div>
                <div className="form-group">
                    <label>Photo</label>
                    <input onChange={props.contactPhoto} type="text" className="form-control" id="exampleFormControlInput1" placeholder="Photo"/>
                </div>
                <div className="form-group">
                    <label>Photo preview</label>
                    <img className="PreviewPhoto" src={props.photoSrc} alt=""/>
                </div>
                <button onClick={props.click} type="button"  className="btn btn-primary">Save</button>
            </form>
        </div>
    );
};

export default Form;
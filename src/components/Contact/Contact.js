import React from 'react';
import './Contact.css';

const Contact = props => {
    return (
        <button onClick={props.click} className="Contact">
            <img className="ContactImage" src={props.photo} alt=""/>
            <p className="ContactName">{props.name}</p>
        </button>
    );
};

export default Contact;
import axios from '../axios-contacts';

export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACTS_ERROR = 'FETCH_CONTACTS_ERROR';
export const FETCH_DELETE_CONTACT = 'FETCH_DELETE_CONTACT';

export const fetchContactsRequest = () => {
    return {type: FETCH_CONTACTS_REQUEST};
};

export const fetchContactsSuccess = data => {
    return {type: FETCH_CONTACTS_SUCCESS, data};
};

export const fetchContactsError = () => {
    return {type: FETCH_CONTACTS_ERROR};
};

export const fetchDeleteContact = () => {
    return {type: FETCH_DELETE_CONTACT}
};

export const fetchContacts = () => {
    return (dispatch) => {
        dispatch(fetchContactsRequest());

        axios.get('/contacts.json').then(response => {
            console.log(response.data);
            const data = [];
            for (let key in response.data) {
                data.push({...response.data[key], id: key})
            }
            dispatch(fetchContactsSuccess(data));
        }, error => {
            dispatch(fetchContactsError());
        });
    }
};

// export const deleteContact = (id, index) => {
//     return (dispatch) => {
//         dispatch(fetchContactsRequest());
//
//         console.log(id, index);
//         axios.delete('/contacts' + id + '.json').finally(() => {
//             dispatch(fetchDeleteContact(id, index));
//         })
//     }
// };
import {FETCH_CONTACTS_SUCCESS, FETCH_DELETE_CONTACT} from "./actions";

const initialStore = {
    contacts: [],
};

const reducer = (state = initialStore, action) => {
    switch(action.type) {
        case FETCH_CONTACTS_SUCCESS:
            return {contacts: action.data};
        case FETCH_DELETE_CONTACT:
            return {contacts: state.contacts.splice(action.index, 1)};
        default:
            return state;
    }
};

export default reducer;
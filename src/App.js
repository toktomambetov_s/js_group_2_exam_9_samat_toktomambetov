import React, { Component, Fragment } from 'react';
import {Switch, Route} from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import AddContactForm from "./containers/AddContactForm/AddContactForm";
import Contacts from "./containers/Contacts/Contacts";

class App extends Component {
    render() {
        return (
            <Fragment>
                <Navbar />

                <Switch>
                    <Route path="/" exact component={Contacts}/>
                    <Route path="/add-quote" component={AddContactForm}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;

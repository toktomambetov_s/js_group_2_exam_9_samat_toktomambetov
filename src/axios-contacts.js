import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://exam-9-83c4a.firebaseio.com/'
});

export default instance;